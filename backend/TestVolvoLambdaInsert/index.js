const AWS = require('aws-sdk');

const dynamo = new AWS.DynamoDB.DocumentClient();
const TableName = "volvo_vehicles"

exports.handler = async (event, context) => {

    let body;
    let statusCode = '200';
    const headers = {
        'Content-Type': 'application/json',
    };

    try {
        const params = {
            TableName,
            Item: JSON.parse(event.body),
            ConditionExpression: 'attribute_not_exists(ChassisNumber)'
        };
        body = await dynamo.put(params).promise();
    } catch (err) {
        statusCode = '400';
        body = err.message;
    } finally {
        body = JSON.stringify(body);
    }

    return {
        statusCode,
        body,
        headers,
    };
};
