const AWS = require('aws-sdk');

const dynamo = new AWS.DynamoDB.DocumentClient();
const TableName = "volvo_vehicles"

console.log("delete")

exports.handler = async (event, context) => {
    console.log(event, context)
    let body;
    let statusCode = '200';
    const headers = {
        'Content-Type': 'application/json',
    };

    try {
        const params = {
            TableName,
            Key:{
              "ChassisNumber": Number(event.queryStringParameters.ChassisNumber),
              "ChassisSeries": event.queryStringParameters.ChassisSeries  
            },
            ConditionExpression: 'attribute_exists(ChassisNumber) AND attribute_exists(ChassisSeries)',
        };
        console.log(params)
        body = await dynamo.delete(params).promise();
    } catch (err) {
        statusCode = '400';
        body = err.message;
    } finally {
        body = JSON.stringify(body);
    }

    return {
        statusCode,
        body,
        headers,
    };
};
