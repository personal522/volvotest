const AWS = require('aws-sdk');

const dynamo = new AWS.DynamoDB.DocumentClient();
const TableName = "volvo_vehicles"

console.log("edit")

exports.handler = async (event, context) => {

    let body;
    let statusCode = '200';
    const headers = {
        'Content-Type': 'application/json',
    };

    try {
        const { Color } = JSON.parse(event.body)
        const params = {
            TableName,
            Key:{
              "ChassisNumber": Number(event.queryStringParameters.ChassisNumber),
              "ChassisSeries": event.queryStringParameters.ChassisSeries  
            },
            UpdateExpression: "set Color = :c",
            ExpressionAttributeValues:{
                ":c": Color
            },
            ConditionExpression: 'attribute_exists(ChassisNumber) AND attribute_exists(ChassisSeries)',
            ReturnValues:"UPDATED_NEW"
        };
        console.log(params)
        body = await dynamo.update(params).promise();
    } catch (err) {
        statusCode = '400';
        body = err.message;
    } finally {
        body = JSON.stringify(body);
    }

    return {
        statusCode,
        body,
        headers,
    };
};
